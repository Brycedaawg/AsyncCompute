project "AsyncCompute"

  language "C++"
  
  kind "ConsoleApp"

  flags "Symbols"
  flags "ExtraWarnings"
  flags "FatalWarnings"
  
  files "project.lua"
  files "src/**"

  includedirs "C:/VulkanSDK/1.1.70.1/Include"
  
  libdirs "C:/VulkanSDK/1.1.70.1/Source/lib"
  
  links "vulkan-1.lib"

  targetdir "build/%{cfg.buildcfg}_%{cfg.platform}"
  
  debugdir "build/%{cfg.buildcfg}_%{cfg.platform}"
  
  objdir "obj/%{cfg.buildcfg}_%{cfg.platform}"
    
  configuration "Release"
    runtime "Release"
    
  configuration "Debug"
    runtime "Debug"