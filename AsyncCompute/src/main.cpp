#include "vulkan/vulkan.h"
#include <assert.h>
#include <malloc.h>
#include <cstdio>
#include <cstdlib>
#include <time.h>
#include <cinttypes>

#define ASSERT_VK_RESULT(result) (void)(result == VK_SUCCESS || (printf("%d", result), __debugbreak(), false))
#define ALLOC_TYPE(type, count) (type*)calloc(count, sizeof(type))
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(*a))

static const char *INSTANCE_ENABLED_LAYER_NAMES[] = { "VK_LAYER_LUNARG_standard_validation" };
static const char *INSTANCE_ENABLED_EXTENSION_NAMES[] = { VK_EXT_DEBUG_REPORT_EXTENSION_NAME };
static const char *DEVICE_ENABLED_EXTENSION_NAMES[] = { "VK_NV_glsl_shader" };
static const uint32_t COMPUTE_QUEUE_COUNT = 4;
static const uint32_t TEST_BUFFER_COUNT = 10;
static const size_t TEST_BUFFER_ELEMENT_SIZE = sizeof(float);
static const size_t TEST_BUFFER_SIZE = TEST_BUFFER_COUNT * TEST_BUFFER_ELEMENT_SIZE;

static const char COMPUTE_SHADER_CODE[] =
{
  "#version 430\n"
  "layout(local_size_x = 32, local_size_y = 32, local_size_z = 1) in;\n"
  "layout(std140, binding = 0) buffer buffer0\n"
  "{\n"
  "  vec4 testBuffer[];\n"
  "};\n"
  "void main()\n"
  "{\n"
  "  float test = 0.0;\n"
  "  for (int i = 0; i < 10000000; ++i)\n"
  "  {\n"
  "    test += 0.00001;\n"
  "  }\n"
  "  testBuffer[0] = vec4(test, 0.0, 0.0, 0.0);\n"
  "}\n"
};

static VKAPI_ATTR VkBool32 VKAPI_CALL _DebugReportCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT /*objectType*/, uint64_t /*object*/, size_t /*location*/, int32_t /*messageCode*/, const char * /*pLayerPrefix*/, const char *pMessage, void * /*pUserData*/)
{
  printf("%s\n\n", pMessage);

  if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
    __debugbreak();

  return VK_FALSE;
}

static inline uint32_t _GetMemoryTypeIndex(VkPhysicalDevice physicalDevice, const VkMemoryRequirements *pMemoryRequirements, VkMemoryPropertyFlags requirementFlags)
{
  VkPhysicalDeviceMemoryProperties memoryProperties;
  vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

  uint32_t memoryTypeBits = pMemoryRequirements->memoryTypeBits;

  for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; ++i)
  {
    if ((memoryTypeBits & 1) && ((requirementFlags & memoryProperties.memoryTypes[i].propertyFlags) == requirementFlags))
      return i;

    memoryTypeBits >>= 1;
  }

  assert(false);
  return 0xFFFFFFFF;
}

int main(int /*argC*/, char ** /*ppArgV*/)
{
  // Create an instance
  VkAllocationCallbacks *pAllocator = nullptr;

  VkApplicationInfo applicationInfo;
  applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  applicationInfo.pNext = nullptr;
  applicationInfo.pApplicationName = "Async Compute";
  applicationInfo.pEngineName = "Async Compute Engine";
  applicationInfo.engineVersion = 1;
  applicationInfo.apiVersion = VK_API_VERSION_1_1;

  VkInstanceCreateInfo instanceCreateInfo;
  instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  instanceCreateInfo.pNext = nullptr;
  instanceCreateInfo.flags = 0;
  instanceCreateInfo.pApplicationInfo = &applicationInfo;
  instanceCreateInfo.enabledLayerCount = ARRAY_SIZE(INSTANCE_ENABLED_LAYER_NAMES);
  instanceCreateInfo.ppEnabledLayerNames = INSTANCE_ENABLED_LAYER_NAMES;
  instanceCreateInfo.enabledExtensionCount = ARRAY_SIZE(INSTANCE_ENABLED_EXTENSION_NAMES);
  instanceCreateInfo.ppEnabledExtensionNames = INSTANCE_ENABLED_EXTENSION_NAMES;

  VkInstance instance;

  ASSERT_VK_RESULT(vkCreateInstance(&instanceCreateInfo, pAllocator, &instance));

  // Register a debug report callback
  PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallbackEXT = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT"));
  PFN_vkDestroyDebugReportCallbackEXT vkDestroyDebugReportCallbackEXT = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT"));

  VkDebugReportCallbackCreateInfoEXT vkDebugReportCallbackCreateInfo;
  vkDebugReportCallbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
  vkDebugReportCallbackCreateInfo.pNext = nullptr;
  vkDebugReportCallbackCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
  vkDebugReportCallbackCreateInfo.pfnCallback = _DebugReportCallback;
  vkDebugReportCallbackCreateInfo.pUserData = nullptr;

  VkDebugReportCallbackEXT debugReportCallback;

  vkCreateDebugReportCallbackEXT(instance, &vkDebugReportCallbackCreateInfo, pAllocator, &debugReportCallback);

  // Enumerate the physical devices and get the queue family properties of the first one
  uint32_t physicalDeviceCount = 0;

  ASSERT_VK_RESULT(vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, nullptr));

  assert(physicalDeviceCount != 0);

  VkPhysicalDevice *pPhysicalDevices = ALLOC_TYPE(VkPhysicalDevice, physicalDeviceCount);

  ASSERT_VK_RESULT(vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, pPhysicalDevices));

  VkPhysicalDevice physicalDevice = pPhysicalDevices[0];

  uint32_t queueFamilyPropertyCount = 0;

  vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, nullptr);

  VkQueueFamilyProperties *pQueueFamilyProperties = ALLOC_TYPE(VkQueueFamilyProperties, queueFamilyPropertyCount);

  vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, pQueueFamilyProperties);

  bool foundComputeQueueFamilyIndex = false;
  uint32_t computeQueueFamilyIndex = 0;

  // Find queue family that only supports compute
  for (uint32_t i = 0; i < queueFamilyPropertyCount; ++i)
  {
    if (pQueueFamilyProperties[i].queueFlags == VK_QUEUE_COMPUTE_BIT && pQueueFamilyProperties[i].queueCount >= COMPUTE_QUEUE_COUNT)
    {
      computeQueueFamilyIndex = i;
      foundComputeQueueFamilyIndex = true;
      break;
    }
  }

  assert(foundComputeQueueFamilyIndex);

  // Create device with queues
  float *pComputeQueuePriorities = ALLOC_TYPE(float, COMPUTE_QUEUE_COUNT);

  for (uint32_t i = 0; i < COMPUTE_QUEUE_COUNT; ++i)
    pComputeQueuePriorities[i] = 0.5f;

  VkDeviceQueueCreateInfo deviceQueueCreateInfo;
  deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
  deviceQueueCreateInfo.pNext = nullptr;
  deviceQueueCreateInfo.flags = 0;
  deviceQueueCreateInfo.queueFamilyIndex = computeQueueFamilyIndex;
  deviceQueueCreateInfo.queueCount = COMPUTE_QUEUE_COUNT;
  deviceQueueCreateInfo.pQueuePriorities = pComputeQueuePriorities;

  VkDeviceCreateInfo deviceCreateInfo;
  deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
  deviceCreateInfo.pNext = nullptr;
  deviceCreateInfo.flags = 0;
  deviceCreateInfo.queueCreateInfoCount = 1;
  deviceCreateInfo.pQueueCreateInfos = &deviceQueueCreateInfo;
  deviceCreateInfo.enabledLayerCount = 0;
  deviceCreateInfo.ppEnabledLayerNames = nullptr;
  deviceCreateInfo.enabledExtensionCount = ARRAY_SIZE(DEVICE_ENABLED_EXTENSION_NAMES);
  deviceCreateInfo.ppEnabledExtensionNames = DEVICE_ENABLED_EXTENSION_NAMES;
  deviceCreateInfo.pEnabledFeatures = nullptr;

  VkDevice device;

  ASSERT_VK_RESULT(vkCreateDevice(physicalDevice, &deviceCreateInfo, pAllocator, &device));

  VkQueue computeQueues[COMPUTE_QUEUE_COUNT];

  for (uint32_t i = 0; i < COMPUTE_QUEUE_COUNT; ++i)
    vkGetDeviceQueue(device, computeQueueFamilyIndex, i, &computeQueues[i]);

  // Create compute pipelines

  VkShaderModule computeShaderModules[COMPUTE_QUEUE_COUNT];
  VkDescriptorSetLayout computeDescriptorSetLayouts[COMPUTE_QUEUE_COUNT];
  VkDescriptorPool computeDescriptorPools[COMPUTE_QUEUE_COUNT];
  VkDescriptorSet computeDescriptorSets[COMPUTE_QUEUE_COUNT];
  VkBuffer computeBuffers[COMPUTE_QUEUE_COUNT];
  VkDeviceMemory computeDeviceMemory[COMPUTE_QUEUE_COUNT];
  VkPipelineLayout computePipelineLayouts[COMPUTE_QUEUE_COUNT];
  VkPipeline computePipelines[COMPUTE_QUEUE_COUNT];
  VkCommandBuffer computeCommandBuffers[COMPUTE_QUEUE_COUNT];
  VkCommandPool computeCommandPools[COMPUTE_QUEUE_COUNT];
  VkFence computeFences[COMPUTE_QUEUE_COUNT];

  for (uint32_t i = 0; i < COMPUTE_QUEUE_COUNT; ++i)
  {
    VkShaderModuleCreateInfo computeShaderModuleCreateInfo;
    computeShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    computeShaderModuleCreateInfo.pNext = nullptr;
    computeShaderModuleCreateInfo.flags = 0;
    computeShaderModuleCreateInfo.codeSize = ARRAY_SIZE(COMPUTE_SHADER_CODE);
    computeShaderModuleCreateInfo.pCode = (uint32_t*)COMPUTE_SHADER_CODE;

    ASSERT_VK_RESULT(vkCreateShaderModule(device, &computeShaderModuleCreateInfo, pAllocator, &computeShaderModules[i]));

    VkDescriptorSetLayoutBinding computeDescriptorSetLayoutBinding;
    computeDescriptorSetLayoutBinding.binding = 0;
    computeDescriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    computeDescriptorSetLayoutBinding.descriptorCount = TEST_BUFFER_COUNT;
    computeDescriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    computeDescriptorSetLayoutBinding.pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutCreateInfo computeDescriptorSetLayoutCreateInfo;
    computeDescriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    computeDescriptorSetLayoutCreateInfo.pNext = nullptr;
    computeDescriptorSetLayoutCreateInfo.flags = 0;
    computeDescriptorSetLayoutCreateInfo.bindingCount = 1;
    computeDescriptorSetLayoutCreateInfo.pBindings = &computeDescriptorSetLayoutBinding;

    ASSERT_VK_RESULT(vkCreateDescriptorSetLayout(device, &computeDescriptorSetLayoutCreateInfo, pAllocator, &computeDescriptorSetLayouts[i]));

    VkDescriptorPoolSize computeDescriptorPoolSize;
    computeDescriptorPoolSize.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    computeDescriptorPoolSize.descriptorCount = TEST_BUFFER_COUNT;

    VkDescriptorPoolCreateInfo computeDescriptorPoolCreateInfo;
    computeDescriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    computeDescriptorPoolCreateInfo.pNext = nullptr;
    computeDescriptorPoolCreateInfo.flags = 0;
    computeDescriptorPoolCreateInfo.maxSets = 1;
    computeDescriptorPoolCreateInfo.poolSizeCount = 1;
    computeDescriptorPoolCreateInfo.pPoolSizes = &computeDescriptorPoolSize;

    ASSERT_VK_RESULT(vkCreateDescriptorPool(device, &computeDescriptorPoolCreateInfo, pAllocator, &computeDescriptorPools[i]));

    VkDescriptorSetAllocateInfo computeDescriptorSetAllocateInfo;
    computeDescriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    computeDescriptorSetAllocateInfo.pNext = nullptr;
    computeDescriptorSetAllocateInfo.descriptorPool = computeDescriptorPools[i];
    computeDescriptorSetAllocateInfo.descriptorSetCount = 1;
    computeDescriptorSetAllocateInfo.pSetLayouts = &computeDescriptorSetLayouts[i];

    ASSERT_VK_RESULT(vkAllocateDescriptorSets(device, &computeDescriptorSetAllocateInfo, &computeDescriptorSets[i]));

    VkBufferCreateInfo computeBufferCreateInfo;
    computeBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    computeBufferCreateInfo.pNext = nullptr;
    computeBufferCreateInfo.flags = 0;
    computeBufferCreateInfo.size = TEST_BUFFER_SIZE;
    computeBufferCreateInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    computeBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    computeBufferCreateInfo.queueFamilyIndexCount = 1;
    computeBufferCreateInfo.pQueueFamilyIndices = &computeQueueFamilyIndex;

    ASSERT_VK_RESULT(vkCreateBuffer(device, &computeBufferCreateInfo, pAllocator, &computeBuffers[i]));

    VkMemoryRequirements computeMemoryRequirements;

    vkGetBufferMemoryRequirements(device, computeBuffers[i], &computeMemoryRequirements);

    VkMemoryAllocateInfo computeMemoryAllocateInfo;
    computeMemoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    computeMemoryAllocateInfo.pNext = nullptr;
    computeMemoryAllocateInfo.allocationSize = computeMemoryRequirements.size;
    computeMemoryAllocateInfo.memoryTypeIndex = _GetMemoryTypeIndex(physicalDevice, &computeMemoryRequirements, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    ASSERT_VK_RESULT(vkAllocateMemory(device, &computeMemoryAllocateInfo, pAllocator, &computeDeviceMemory[i]));

    ASSERT_VK_RESULT(vkBindBufferMemory(device, computeBuffers[i], computeDeviceMemory[i], 0));

    VkDescriptorBufferInfo computeDescriptorBufferInfo;
    computeDescriptorBufferInfo.buffer = computeBuffers[i];
    computeDescriptorBufferInfo.offset = 0;
    computeDescriptorBufferInfo.range = VK_WHOLE_SIZE;

    VkWriteDescriptorSet computeWriteDescriptorSet;
    computeWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    computeWriteDescriptorSet.pNext = nullptr;
    computeWriteDescriptorSet.dstSet = computeDescriptorSets[i];
    computeWriteDescriptorSet.dstBinding = 0;
    computeWriteDescriptorSet.dstArrayElement = 0;
    computeWriteDescriptorSet.descriptorCount = 1;
    computeWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    computeWriteDescriptorSet.pImageInfo = nullptr;
    computeWriteDescriptorSet.pBufferInfo = &computeDescriptorBufferInfo;
    computeWriteDescriptorSet.pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(device, 1, &computeWriteDescriptorSet, 0, nullptr);

    VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo;
    pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCreateInfo.pNext = nullptr;
    pipelineLayoutCreateInfo.flags = 0;
    pipelineLayoutCreateInfo.setLayoutCount = 1;
    pipelineLayoutCreateInfo.pSetLayouts = &computeDescriptorSetLayouts[i];
    pipelineLayoutCreateInfo.pushConstantRangeCount = 0;
    pipelineLayoutCreateInfo.pPushConstantRanges = nullptr;

    ASSERT_VK_RESULT(vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, pAllocator, &computePipelineLayouts[i]));

    VkComputePipelineCreateInfo computePipelineCreateInfo;
    computePipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
    computePipelineCreateInfo.pNext = nullptr;
    computePipelineCreateInfo.flags = 0;
    computePipelineCreateInfo.stage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    computePipelineCreateInfo.stage.pNext = nullptr;
    computePipelineCreateInfo.stage.flags = 0;
    computePipelineCreateInfo.stage.stage = VK_SHADER_STAGE_COMPUTE_BIT;
    computePipelineCreateInfo.stage.module = computeShaderModules[i];
    computePipelineCreateInfo.stage.pName = "main";
    computePipelineCreateInfo.stage.pSpecializationInfo = nullptr;
    computePipelineCreateInfo.layout = computePipelineLayouts[i];
    computePipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
    computePipelineCreateInfo.basePipelineIndex = 0;

    ASSERT_VK_RESULT(vkCreateComputePipelines(device, VK_NULL_HANDLE, 1, &computePipelineCreateInfo, pAllocator, &computePipelines[i]));

    VkCommandPoolCreateInfo computeCommandPoolCreateInfo;
    computeCommandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    computeCommandPoolCreateInfo.pNext = nullptr;
    computeCommandPoolCreateInfo.flags = 0;
    computeCommandPoolCreateInfo.queueFamilyIndex = computeQueueFamilyIndex;

    ASSERT_VK_RESULT(vkCreateCommandPool(device, &computeCommandPoolCreateInfo, pAllocator, &computeCommandPools[i]));

    VkCommandBufferAllocateInfo computeCommandBufferAllocateInfo;
    computeCommandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    computeCommandBufferAllocateInfo.pNext = nullptr;
    computeCommandBufferAllocateInfo.commandPool = computeCommandPools[i];
    computeCommandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    computeCommandBufferAllocateInfo.commandBufferCount = 1;

    ASSERT_VK_RESULT(vkAllocateCommandBuffers(device, &computeCommandBufferAllocateInfo, &computeCommandBuffers[i]));

    VkCommandBufferBeginInfo computeCommandBufferBeginInfo;
    computeCommandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    computeCommandBufferBeginInfo.pNext = nullptr;
    computeCommandBufferBeginInfo.flags = 0;
    computeCommandBufferBeginInfo.pInheritanceInfo = nullptr;

    ASSERT_VK_RESULT(vkBeginCommandBuffer(computeCommandBuffers[i], &computeCommandBufferBeginInfo));

    vkCmdBindPipeline(computeCommandBuffers[i], VK_PIPELINE_BIND_POINT_COMPUTE, computePipelines[i]);
    vkCmdBindDescriptorSets(computeCommandBuffers[i], VK_PIPELINE_BIND_POINT_COMPUTE, computePipelineLayouts[i], 0, 1, &computeDescriptorSets[i], 0, nullptr);
    vkCmdDispatch(computeCommandBuffers[i], 1, 1, 1);

    ASSERT_VK_RESULT(vkEndCommandBuffer(computeCommandBuffers[i]));

    VkFenceCreateInfo computeFenceCreateInfo;
    computeFenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    computeFenceCreateInfo.pNext = nullptr;
    computeFenceCreateInfo.flags = 0;

    ASSERT_VK_RESULT(vkCreateFence(device, &computeFenceCreateInfo, pAllocator, &computeFences[i]));
  }

  for (uint32_t asyncComputes = 1; asyncComputes < COMPUTE_QUEUE_COUNT + 1; ++asyncComputes)
  {
    clock_t startTime = clock();

    for (uint32_t i = 0; i < asyncComputes; ++i)
    {
      VkSubmitInfo computeSubmitInfo;
      computeSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
      computeSubmitInfo.pNext = nullptr;
      computeSubmitInfo.waitSemaphoreCount = 0;
      computeSubmitInfo.pWaitSemaphores = nullptr;
      computeSubmitInfo.pWaitDstStageMask = 0;
      computeSubmitInfo.commandBufferCount = 1;
      computeSubmitInfo.pCommandBuffers = &computeCommandBuffers[i];
      computeSubmitInfo.signalSemaphoreCount = 0;
      computeSubmitInfo.pSignalSemaphores = nullptr;

      ASSERT_VK_RESULT(vkQueueSubmit(computeQueues[i], 1, &computeSubmitInfo, computeFences[i]));
    }

    ASSERT_VK_RESULT(vkWaitForFences(device, asyncComputes, computeFences, true, 10000000000));
    ASSERT_VK_RESULT(vkResetFences(device, asyncComputes, computeFences));

    clock_t endTime = clock();

    uint64_t elapsedMilliseconds = (endTime - startTime) * 1000 / CLOCKS_PER_SEC;

    printf("Elapsed Milliseconds (%" PRIu32 " async computes): %" PRIu64 "\n", asyncComputes, elapsedMilliseconds);
  }

  for (uint32_t i = 0; i < COMPUTE_QUEUE_COUNT; ++i)
  {
    vkDestroyFence(device, computeFences[i], pAllocator);
    vkFreeCommandBuffers(device, computeCommandPools[i], 1, &computeCommandBuffers[i]);
    vkDestroyCommandPool(device, computeCommandPools[i], pAllocator);
    vkDestroyPipeline(device, computePipelines[i], pAllocator);
    vkDestroyPipelineLayout(device, computePipelineLayouts[i], pAllocator);
    vkDestroyDescriptorSetLayout(device, computeDescriptorSetLayouts[i], pAllocator);
    vkFreeMemory(device, computeDeviceMemory[i], pAllocator);
    vkDestroyBuffer(device, computeBuffers[i], pAllocator);
    vkDestroyDescriptorPool(device, computeDescriptorPools[i], pAllocator);
    vkDestroyShaderModule(device, computeShaderModules[i], pAllocator);
  }

  vkDestroyDevice(device, pAllocator);
  free(pComputeQueuePriorities);
  free(pQueueFamilyProperties);
  free(pPhysicalDevices);
  vkDestroyDebugReportCallbackEXT(instance, debugReportCallback, pAllocator);
  vkDestroyInstance(instance, pAllocator);

  return 0;
}