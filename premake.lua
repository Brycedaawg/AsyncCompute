solution "AsyncCompute"

  configurations "Debug"
  configurations "Release"

  platforms "x32"
  platforms "x64"
  
  defaultplatform "x64"

include "AsyncCompute/project.lua"